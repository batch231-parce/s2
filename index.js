// Without the use of objects, our students from before would be organized as follows if we are to record additional information about them

// Spaghetti code - code does not organized enough hat it becomes hard to work with.

// Encapsulation - organize related information (properties) and behavior () to belong to a single entity.

//create student one
/*let studentOneName = 'Tony';
let studentOneEmail = 'starksindustries@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Peter';
let studentTwoEmail = 'spideyman@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Wanda';
let studentThreeEmail = 'scarlettMaximoff@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Steve';
let studentFourEmail = 'captainRogers@mail.com';
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}*/


// Encapsulate the folllowing information into 4 student objects using object literals.
let studentOne = {
	name: "Tony",
	email: "starksindustries@mail.com",
	grades: [89, 84, 78, 88],

	// add the functionalities available to a student as object methods
		// keyword "this" refers to the object encapsulating the method where the "this" is called

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
		console.log(`${this.email} has logged out`);
	},

	listGrades(grades) {
		this.grades.forEach(grades => {
			console.log(`${this.name}'s quarterly grade average are ${this.grades}`);
		})
	},

	// [89, 84, 78, 88]
	computeAverage(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},

	// MINI ACTIVITY
	// Create a method that will return true if average grade is >=85, false otherwise.
	/*willPass(computeAverage) {
		if (computeAverage >= 85) return true;
		return false;
	},*/

	willPass(){
		// You can call methods inside an object
		return this.computeAverage() >= 85 ? true: false;
	},

	// Syntax for ternary operator: condition ? value if condition is true: value if condition is false

	// method that returns true if the student has passed and their average is equal or greater than 90, otherwise false.
	willPassWithHonors(){
		return (this.willPass() && this.computeAverage() >= 90) ? true : false
	}
}
console.log(`student one's name is ${studentOne.name}`);
console.log(this); // result: global window object

// MINI - ACTIVITY
let studentTwo = {
	name: "Peter",
	email: "spideyman@mail.com",
	grades: [78, 82, 79, 85],

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
		console.log(`${this.email} has logged out`);
	},

	listGrades(grades) {
		this.grades.forEach(grades => {
			console.log(`${this.name}'s quarterly grade average are ${this.grades}`);
		})
	},

	computeAverage(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},

	willPass(){
		return this.computeAverage() >= 85 ? true: false;
	},

	willPassWithHonors(){
		return (this.willPass() && this.computeAverage() >= 90) ? true : false
	}
}

let studentThree = {
	name: "Wanda",
	email: "scarlettMaximoff@mail.com",
	grades: [87, 89, 91, 93],

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
		console.log(`${this.email} has logged out`);
	},

	listGrades(grades) {
		this.grades.forEach(grades => {
			console.log(`${this.name}'s quarterly grade average are ${this.grades}`);
		})
	},

	computeAverage(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},

	willPass(){
		return this.computeAverage() >= 85 ? true: false;
	},

	willPassWithHonors(){
		return (this.willPass() && this.computeAverage() >= 90) ? true : false
	}
}

let studentFour = {
	name: "Steve",
	email: "captainRogers@mail.com",
	grades: [91, 89, 92, 93],

	login(){
		console.log(`${this.email} has logged in`);
	},

	logout(email){
		console.log(`${this.email} has logged out`);
	},

	listGrades(grades) {
		this.grades.forEach(grades => {
			console.log(`${this.name}'s quarterly grade average are ${this.grades}`);
		})
	},

	computeAverage(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		return sum / 4;
	},

	willPass(){
		return this.computeAverage() >= 85 ? true: false;
	},

	willPassWithHonors(){
		return (this.willPass() && this.computeAverage() >= 90) ? true : false;
	}
}

/*
// MINI ACTIIVITY

1. What is the term given to unorganized code that's very hard to work with?
   Answer: Spaghetti code

2. How are object literals written in JS?
	Answer: {}

3. What do you call the concept of organizing information and functionality to belong to an object?
   Answer: Encapsulation

4. If student1 has a method named enroll(), how would you invoke it?
	Answer: studentOne.enroll()

5. True or False: Objects can have objects as properties.
	Answer: True

6. What does the this keyword refer to if used in an arrow function method?
	Answer: this keyword represents the object that called the function.
	- Global window object

7. True or False: A method can have no parameters and still work.
 	Answer: True

8. True or False: Arrays can have objects as elements.
   Answer: True

9. True or False: Arrays are objects.
	Answer: True

10. True or False: Objects can have arrays as properties.
	Answer: True
*/

const classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],

	countHonorStudents(){
		let result = 0;
		this.students.forEach(student => {
			if(student.willPassWithHonors()){
				result++;
			}
		})
		return result;
	},

	// Function Coding Activity: (1 hr)

	// 1. Create a method for the object classOf1A named honorsPercentage() 
	// that will return the % of honor students from the batch's total number of students.

	honorsPercentage(){
		let percentage = (this.countHonorStudents() / this.students.length) * 100;
		console.log(`${percentage}%`);
	},

	// 2. Create a method for the object classOf1A named retrieveHonorStudentInfo() 
	// that will return all honor students' emails and ave. grades as an array of objects.

	retrieveHonorStudentInfo(){
		let honors = [];
		this.students.forEach(student => {
			if(student.willPassWithHonors()){
				return honors.push({
					email: student.email, 
					average: student.computeAverage()
				})
			}
		})
		return honors;
	},

	// 3. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() 
	// that will return all honor students' emails and ave. grades as an array of objects sorted 
	// in descending order based on their grade averages.

	sortHonorStudentsByGradeDesc(){
		let sortHonors = this.retrieveHonorStudentInfo();
		sortHonors.sort((valA, valB) => valB.average - valA.average);	
		return sortHonors;
	}
}

